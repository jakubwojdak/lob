<?php
/*
Plugin Name: Search Results Filter
Plugin URI: ?
Description: Allows you to add the filter, to narrow length of contents on search results page
Version: 1.1.0
Author: Jakub Wojdak
*/

$search_results_filter_plugin_query_string_values = array();
$search_results_filter_plugin_phrase_to_search = null;

function search_results_filter_plugin_set_phrase_to_search($search_phrase)
{
    global $search_results_filter_plugin_phrase_to_search;
    $search_results_filter_plugin_phrase_to_search = trim($search_phrase);
}

function search_results_filter_plugin_reset_phrase_to_search()
{
    global $search_results_filter_plugin_phrase_to_search;
    $search_results_filter_plugin_phrase_to_search = null;
}

function search_results_filter_plugin_set_query_string_values()
{
    global $query_string;
    global $search_results_filter_plugin_query_string_values;

    $query_args = explode("&", $query_string);
    if (is_array($query_args)) {
        foreach ($query_args as $string) {
            $query_split = explode("=", $string);
            $search_results_filter_plugin_query_string_values[$query_split[0]] = trim(urldecode($query_split[1]));
        }
    }
}

function search_results_filter_plugin_mark_content($content, $other_words, $check_all_words, $show_dots, $tag, $class)
{
    global $search_results_filter_plugin_query_string_values;
    global $search_results_filter_plugin_phrase_to_search;

    $search_string = '';

    if ($search_results_filter_plugin_phrase_to_search != null) {

        $search_string = $search_results_filter_plugin_phrase_to_search;

    } elseif (count($search_results_filter_plugin_query_string_values) === 0) {

        search_results_filter_plugin_set_query_string_values();
        $search_string = $search_results_filter_plugin_query_string_values['s'];

    } elseif (isset($search_results_filter_plugin_query_string_values['s'])) {

        $search_string = $search_results_filter_plugin_query_string_values['s'];

    }

    if ($search_string == '') {
        return $content;
    }

    $content = strip_tags($content);
    $content = preg_replace('@(\t|\r|\n)@', ' ', $content);

    $result_words_array = array();
    $found_index = -1;
    $after = $before = $other_words / 2;
    if ($before > (int)$before) {
        $after = ((int)$after) + 1;
        $before = (int)$before;
    }

    $searching_words = array();
    $words_to_search = array($search_string);

    if (stripos($content, $words_to_search[0]) === false) {
        $words_to_search = explode(' ', $words_to_search[0]);
    }

    foreach($words_to_search as $single_word){
        $iterator = 0;
        if (strlen($single_word) > 2) {
            $word_position = stripos($content, $single_word);
            if ($word_position !== false) {
                $original_word = substr($content, $word_position, strlen($single_word));
                $searching_words['&finded' . $iterator . '&'] = $original_word;
                $content = str_replace($original_word, '&finded' . $iterator . '&', $content);
                $iterator++;
            }
        }
    }

    $all_words_array = explode(' ', $content);

    foreach ($all_words_array as $index => $word) {
        if (false !== strpos(strtolower($word), '&finded0&')) {
            $found_index = $index;
            break;
        }
    }

    if ($found_index - $before < 0) {
        $after += $before - $found_index;
        $before = $found_index;
    } elseif ($found_index + $after > count($all_words_array) - 1) {
        $before += $found_index + $after - (count($all_words_array) - 1);
        if ($found_index - $before < 0) {
            $before = $found_index;
        }
    }
    setlocale(LC_ALL, 'pl_PL.UTF8');
    Mb_Internal_Encoding( 'UTF-8' );
    if ($show_dots && $found_index > -1 && $found_index - $before > 0) {
        $result_words_array[] = '...';
    }

    for ($i = $found_index - $before; $i <= $found_index + $after; $i++) {
        if (isset($all_words_array[$i])) {
            $w = $all_words_array[$i];
            foreach ($searching_words as $key_to_replace => $word) {
                if (false !== strpos($w, $key_to_replace)) {
                    if ($check_all_words) {
                        global $callback_tag;
                        global $callback_class;
                        global $callback_key_to_replace;
                        global $callback_searching_words;
                        $callback_tag = $tag;
                        $callback_class = $class;
                        $callback_key_to_replace = $key_to_replace;
                        $callback_searching_words = $searching_words[$key_to_replace];
                        $w = preg_replace_callback(
                            '/[\w]*' . str_replace('/', '\/', $key_to_replace) . '[\w]*/iu',
                            create_function('$matches', 'global $callback_tag;
                                global $callback_class;
                                global $callback_key_to_replace;
                                global $callback_searching_words;
                                return \'<\' . $callback_tag .
                                    ($callback_class != \'\' ? \' class="\' . $callback_class . \'"\' : \'\') .
                                    \'>\' . str_replace(
                                    $callback_key_to_replace
                                    , $callback_searching_words
                                    , $matches[0]
                                    ) .
                                    \'</\' . $callback_tag . \'>\';'),
                            $w
                        );
                    } else {
                        $w = str_replace(
                            $key_to_replace,
                            '<' . $tag .
                                ($class != '' ? ' class="' . $class . '"' : '') .
                                '>'
                                . $searching_words[$key_to_replace] .
                                '</' . $tag . '>',
                            $w
                        );
                    }
                }
            }
            $result_words_array[] = $w;
        } else {
            break;
        }

    }

    if ($show_dots && $found_index > -1 && $found_index + $after < count($all_words_array) - 1) {
        $result_words_array[] = '...';
    }

    return implode(' ', $result_words_array);
}

function search_results_filter_plugin_filter_post_content(
    $content,
    $other_words = 10,
    $check_all_words = false,
    $show_dots = true,
    $tag = 'strong',
    $class = 'query-searched'
)
{
    global $search_results_filter_plugin_phrase_to_search;
    if ($search_results_filter_plugin_phrase_to_search !== null || (is_search() && !is_admin())) {
        $content = search_results_filter_plugin_mark_content($content, $other_words, $check_all_words, $show_dots, $tag, $class);
    }
    return $content;
}

add_filter('the_title', 'search_results_filter_plugin_filter_post_content', 10);
add_filter('the_content', 'search_results_filter_plugin_filter_post_content', 10);
add_filter('the_excerpt', 'search_results_filter_plugin_filter_post_content', 10);
