<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 23.08.13
 * Time: 10:58
 * To change this template use File | Settings | File Templates.
 */
/*
 * Template Name: O LOB - Partnerzy i patroni
 */
ob_start();

echo '<div class="partners-container"><h1>Partnerzy i patroni</h1>' . "\n";

$partner_terms = array(
    'partner-honorowy' => array('name' => 'Partner honorowy'),
    'partnerzy' => array('name' => 'Partnerzy'),
    'partner-medialny' => array('name' => 'Partner medialny')
);

foreach ($partner_terms as $term => $term_data) {
    $args = array('post_type' => 'partner',
        'posts_per_page' => -1,
        'orderby' => 'menu_order',
        'tax_query' => array(
            array(
                'taxonomy' => 'partners_group',
                'field' => 'slug',
                //not all terms in one, because returned post don't have information which terms is assigned
                'terms' => array($term)
            )
        )
    );
    $partners = get_posts($args);
    if (count($partners)):
        echo '<h2>'.$term_data['name'].'</h2>';
        echo '<ul>';
        foreach ($partners as $partner) {
            $link = get_post_meta($partner->ID, 'url', true);
            $image_info = wp_get_attachment_image_src(get_post_thumbnail_id($partner->ID), 'partner-logo');
            $html = '<img src="' . $image_info[0] . '" alt="' . $partner->post_title . '" />';
            if ($link != '') {
                $html = '<li><a target="_blank" href="' . $link . '">' . $html . '</a></li>';
            } else {
                $html = '<li><span>' . $html . '</span></li>';
            }
            echo $html;
        }
        echo '</ul>';
    endif;
}

echo '</div>'; //end calendar-container

$ob_content = ob_get_contents();
ob_end_clean();

include(get_stylesheet_directory() . '/template-about-lob.php');