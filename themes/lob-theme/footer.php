<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 12.08.13
 * Time: 13:10
 * To change this template use File | Settings | File Templates.
 */
global $block_ids;
$icons_url = get_bloginfo('template_url') . '/images/footer-icons';
$locations = get_nav_menu_locations();

if (count($lang_pages)) {
    foreach ($lang_pages as $lang_page) {
        echo '<span class="root-font lang"><a href="' . $lang_page['permalink'] . '">' .
            $lang_page['title'] . '</a></span>';
    }
}
?>
<div class="sticky-footer">
    <div class="sticky-footer-content">
        <div class="roll-button roll-up"><span>rozwiń menu</span></div>
        <div class="footer-menu">
            <div class="headers">
                <div class="col">
                    <a href="/">
                        <img src="<?php echo $icons_url; ?>/btn-home.png" alt=""/>
                        Strona główna</a>
                </div>
                <div class="col">
                    <a href="<?php
                    echo get_permalink(get_first_child_page_id($block_ids['about-lob']));
                    ?>">
                        <img src="<?php echo $icons_url; ?>/btn-about-lob.png" alt=""/>
                        O LOB</a>
                </div>
                <div class="col">
                    <a href="<?php
                    echo get_permalink(get_first_child_page_id($block_ids['lob-for-students']));
                    ?>">
                        <img src="<?php echo $icons_url; ?>/btn-for-students.png" alt=""/>
                        LOB dla studenta</a>
                </div>
                <div class="col" style="min-width:174px">
                    <a href="<?php
                    echo get_permalink(get_first_child_page_id($block_ids['ambassadors-csr']));
                    ?>">
                        <img src="<?php echo $icons_url; ?>/btn-ambasadors-csr.png" alt=""/>
                        <span style="display: block;width: 120px;float:left;"
                              class="syllable-container">ambasadorzy CSR</span></a>
                </div>
                <div class="col">
                    <a href="<?php echo get_permalink($block_ids['contact']); ?>">
                        <img src="<?php echo $icons_url; ?>/btn-contact.png" alt=""/>
                        Kontakt</a>
                </div>
                <div class="col">
                    <a target="_blank" href="https://www.facebook.com/LigaOdpowiedzialnegoBiznesu">
                        <img src="<?php echo $icons_url; ?>/btn-facebook.png" alt=""/>
                    </a>
                    <a target="_blank" href="https://twitter.com/LigaOdpBiznesu">
                        <img src="<?php echo $icons_url; ?>/btn-twitter.png" alt=""/>
                    </a>
                    <a target="_blank" href="https://plus.google.com/109850047115655791091/posts">
                        <img src="<?php echo $icons_url; ?>/btn-google-plusone.png" alt=""/>
                    </a>
                </div>
            </div>
            <div class="submenus-container">
                <div class="col">
                    <div class="submenus-hide-content">

                    </div>
                </div>
                <div class="col">
                    <div class="submenus-hide-content">
                        <?php
                        if (isset($locations['about_lob_footer'])) {
                            $menu_items = wp_get_nav_menu_items($locations['about_lob_footer']);
                            if ($menu_items) {
                                echo '<ul>';
                                foreach ($menu_items as $menu_item) {
                                    echo '<li><a href="' .
                                        get_permalink($menu_item->object_id) . '">' .
                                        $menu_item->title . '</a></li>';
                                }
                                echo '</ul>';
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="col">
                    <div class="submenus-hide-content">
                        <?php
                        if (isset($locations['for_students_footer'])) {
                            $menu_items = wp_get_nav_menu_items($locations['for_students_footer']);
                            if ($menu_items) {
                                echo '<ul>';
                                foreach ($menu_items as $menu_item) {
                                    $page_id = $menu_item->object_id;
                                    if ($menu_item->title == 'Archiwum') {
                                        $page_id = get_first_child_page_id($menu_item->object_id);
                                    }
                                    echo '<li><a href="' .
                                        get_permalink($page_id) . '">' .
                                        $menu_item->title . '</a></li>';
                                }
                                echo '</ul>';
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="col">
                    <div class="submenus-hide-content">
                        <?php
                        if (isset($locations['ambassadors_footer'])) {
                            $menu_items = wp_get_nav_menu_items($locations['ambassadors_footer']);
                            if ($menu_items) {
                                echo '<ul>';
                                foreach ($menu_items as $menu_item) {
                                    echo '<li><a href="' .
                                        get_permalink($menu_item->object_id) . '">' .
                                        $menu_item->title . '</a></li>';
                                }
                                echo '</ul>';
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="col col-contact">
                    <div class="submenus-hide-content">
                        <div>
                            <p>Copyright:<br/>
                                Liga Odpowiedzialnego Biznesu<br/>
                                All rights reserved</p>

                            <p>Grafika:
                                <a href="http://logotomia.com.pl">Logotomia</a></p>

                            <p>Wykonanie:
                                <a href="http://laboratorium.ee">Laboratorium EE</a></p>
                        </div>
                    </div>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
(jQuery)(function ($) {

    var sessionStorage = window.sessionStorage;

    if (navigator.appVersion.indexOf("WebKit") > 0 && navigator.userAgent.indexOf("Linux") > 0) {
        //For Gotham line-height font correction in WebKit
        $('html').addClass('web-kit');
    }

    $(document).ready(function () {
        //footer submenu hide/show service
        $('.sticky-footer-content > .roll-button').on('click', function () {
            var $this = $(this);
            var span = $('span', $this);
            setFooterHeight(true);

            $this.toggleClass('roll-up');
            if ($this.hasClass('roll-up')) {
                span.html('rozwiń menu');
            } else {
                span.html('zwiń menu');
            }

        });

        function setFooterHeight(changeState) {
            //changeState - roll or up menu. If false, this means refresh dimensions
            var maxSubmenusHeight = 0;
            var paddingBottomSubmenus = 30;
            var pageContainer = $('.page-container');
            var actualFooterHeight = $('.sticky-footer').height();
            var submenusHideContent = $('.submenus-hide-content');
            submenusHideContent.each(function () {
                var submenuHeight = $('> *', $(this)).height();
                if (submenuHeight > maxSubmenusHeight) {
                    maxSubmenusHeight = submenuHeight;

                }
            });
            if (changeState) {
                if (submenusHideContent.first().height() > 0) {//footer is rollup
                    submenusHideContent.css('height', '0px');
                    pageContainer.css('margin-bottom', ((
                        actualFooterHeight - maxSubmenusHeight - paddingBottomSubmenus
                        ) + 50) + 'px');
                    maxSubmenusHeight = 0;
                } else {
                    submenusHideContent.css('height', maxSubmenusHeight + paddingBottomSubmenus + 'px');
                    pageContainer.css('margin-bottom', ((
                        actualFooterHeight + maxSubmenusHeight + paddingBottomSubmenus
                        ) + 50) + 'px');
                }
            } else {
                if (submenusHideContent.first().height() > 0) {//footer is rollup
                    submenusHideContent.css('height', maxSubmenusHeight + paddingBottomSubmenus + 'px');
                }
                pageContainer.css('margin-bottom', (actualFooterHeight + 50) + 'px');
            }
            //to extort update margin bottom value after add transition to page-container element
            pageContainer.css('margin-bottom');
        }

        //font size service
        var body_font_size = parseInt($('html').css('font-size'), 10);
        var fontSizeToggler = $('.font-size-toggler');

        fontSizeToggler.data('font-size-interval', 4);
        fontSizeToggler.data('initial-font-size', body_font_size);
        fontSizeToggler.attr('data-actual-font-size-level', 0);

        //set value from session
        if (sessionStorage.actual_font_size_level != null) {
            fontSizeToggler.attr('data-actual-font-size-level', sessionStorage.actual_font_size_level);
            changeFontSize();
        }

        $('.root-font').on('click', changeFontSize);

        function changeFontSize() {
            var $this = $(this);

            var html_element = $('html');

            var actualBodyFontSizeLevel = parseInt(fontSizeToggler.attr('data-actual-font-size-level'), 10);
            var initialBodyFontSize = fontSizeToggler.data('initial-font-size');
            var fontSizeInterval = fontSizeToggler.data('font-size-interval');
            var newFontSize = initialBodyFontSize;
            if ($this.hasClass('higher')) {
                actualBodyFontSizeLevel++;
                newFontSize = initialBodyFontSize + (actualBodyFontSizeLevel * fontSizeInterval);
            } else if ($this.hasClass('smaller')) {
                actualBodyFontSizeLevel--;
                newFontSize = initialBodyFontSize + (actualBodyFontSizeLevel * fontSizeInterval);
            } else if ($this.hasClass('normal')) {
                actualBodyFontSizeLevel = 0;
                newFontSize = initialBodyFontSize;
            } else { // getting from session
                newFontSize = initialBodyFontSize + (actualBodyFontSizeLevel * fontSizeInterval);
            }

            if (
                newFontSize > initialBodyFontSize + 2.5 * fontSizeInterval
                    ||
                    newFontSize < initialBodyFontSize
                ) {
                return;
            }

            fontSizeToggler.attr('data-actual-font-size-level', actualBodyFontSizeLevel);
            sessionStorage.actual_font_size_level = actualBodyFontSizeLevel;

            $('body').attr('data-level', actualBodyFontSizeLevel);

            html_element.css('font-size', newFontSize + 'px');
            if (newFontSize > initialBodyFontSize) {// set last column width
                $('.footer-menu .col:last-child').css('min-width', 'auto');
            } else {
                $('.footer-menu .col:last-child').css('min-width', '128px');
            }

            //refresh leftbar elements dimmension
            $('.left-sidebar').removeClass('sidebar-menu-transition-on');
            refreshLeftbarElementsHeight();
            $('.left-sidebar').addClass('sidebar-menu-transition-on');

            setFooterHeight(false);

            refreshSyllableSepaprators();

            resetAmbassadorsListDimmension();

            if (typeof Mapa != 'undefined') {
                Mapa.refreshContainerOptions();
            }

        }

        function changeContrastState() {
            var $this = $('.bw-box');

            var blackwhiteSupports = [];
            blackwhiteSupports[0] = $('.content-size-listener *');
            blackwhiteSupports[0] = $('.ambassador-tile');
            blackwhiteSupports[1] = $('body');
            blackwhiteSupports[2] = $this;
            blackwhiteSupports[3] = $('.left-sidebar *');
            blackwhiteSupports[4] = $('.home-tiles');

            if ($this.hasClass('blackwhite')) {
                var blackwhite_collection = $('.blackwhite');
                blackwhiteSupports[1].removeClass('high-img-contrast');
                blackwhite_collection.removeClass('blackwhite');
            } else {
                blackwhiteSupports[1].addClass('high-img-contrast');
                for (var index in blackwhiteSupports) {
                    blackwhiteSupports[index].addClass('blackwhite');
                }
            }

            if ($this.hasClass('blackwhite')) {
                sessionStorage.contrast_state = 'high';
            } else {
                sessionStorage.contrast_state = 'low';
            }
        }

        //contrast service
        $('.bw-box').on('click', changeContrastState);

        if (sessionStorage.contrast_state != null) {
            if (sessionStorage.contrast_state == 'high') {
                changeContrastState();
            }

        }

        setFooterHeight(false);
        $('.page-container').addClass('page-container-transition');

        var parentContainer = $('.current-site').parent().parent();
        refreshLeftbarContainersHeight(parentContainer);

    });

    /* start leftbar functionality */

    $('.sidebar-menu ul').addClass('rollup');
    $('.current-site').parents().each(function () {
        $(this).removeClass('rollup');
    });

    $("a[data-container-id]").each(function () {//link as activators rollup and rolldown lists
        var $this = $(this);
        var ul = $('> ul', $this.parent());

        $this.on('click', function () {
            var activator = $(this);
            var height = getContentElementHeight(ul);
            if (ul.hasClass('rollup')) {//height is also 0px
                ul.parents().each(function () {
                    var parent = $(this);
                    if (parent.get(0).tagName === 'UL' && !parent.hasClass('sidebar-menu')) {
                        parent.css('height', parent.height() + height);
                    }
                    $(this).removeClass('rollup');
                    activator.addClass('rolldown');
                });
                ul.css('height', height + 'px');
            } else {
                ul.parents().each(function () {
                    var parent = $(this);
                    if (parent.get(0).tagName === 'UL' && !parent.hasClass('sidebar-menu')) {
                        parent.css('height', parent.height() - height);
                    }
                });
                ul.css('height', '0px');
                activator.removeClass('rolldown');
            }
            ul.toggleClass('rollup');
            return false;
        });
    });

    //$('.left-sidebar > ul > li > a').off('click');//block action on root link sidebar element

    function getContentElementHeight(element) {
        var height = 0;
        $('> *', element).each(function () {
            height += $(this).height();
        })
        return height;
    }

    $('.page-icon').click(function () {
        var parentContainer = $('.current-site').parent().parent();
        refreshLeftbarContainersHeight(parentContainer);
    })

    function refreshLeftbarElementsHeight() {
        //find container who potentially don't have subcontainers
        $('.left-sidebar ul > li > a').each(function () {
            var $this = $(this);
            var subContainerNr = $this.data('container-id');
            if (subContainerNr == null) {
                var parentContainer = $this.parent().parent();
                refreshLeftbarContainersHeight(parentContainer);//follow top hierarchy
            }
        });
    }

    function refreshLeftbarContainersHeight(container) {//follow top hierarchy
        if (!container.hasClass('rollup')) {
            var containerHeight = getContentElementHeight(container);
            var aActivator = $('> a', container.parent());
            aActivator.addClass('rolldown');
            container.css('height', containerHeight + 'px');
            container.css('height');
        }
        var upperContainer = container.parent().parent();
        if (upperContainer.get(0) && upperContainer.get(0).tagName === 'UL' && !upperContainer.hasClass('sidebar-menu')) {
            refreshLeftbarContainersHeight(upperContainer);
        }
    }

    /* end leftbar functionality */

    //syllables functionality
    $(document).ready(function () {
        var specialSyllableWords = [];
        specialSyllableWords['Odpowiedzialnego'] = ['Odpo', 'wie', 'dzial', 'nego'];
        specialSyllableWords['odpowiedzialny?'] = ['odpo', 'wie', 'dzial', 'ny?'];
        specialSyllableWords['Przedsiębiorczy?'] = ['Przed', 'się', 'bior', 'czy?'];
        specialSyllableWords['ambasadorzy'] = ['amba', 'sa', 'do', 'rzy'];

        $('.syllable-container').each(function () {
            var $this = $(this);
            var textInContainer = $this.html();

            for (var word in specialSyllableWords) {
                var first = true;
                var syllableWord = '';
                for (var syllable in specialSyllableWords[word]) {

                    if (first) {
                        first = false;
                        syllableWord += specialSyllableWords[word][syllable];
                        continue;
                    }
                    syllableWord += '<span class="syllable">' + specialSyllableWords[word][syllable] + '</span>';
                }
                textInContainer = textInContainer.replace(word, syllableWord, 'g');
                $this.html(textInContainer);
            }
        });
    });

    function refreshSyllableSepaprators() {
        $('.syllable-container > .syllable-separator').remove();

        $('.syllable').each(function () {
            var $this = $(this);
            var parent = $this.parent();
            if ($this.offset().left - parent.offset().left == 0) {
                var syllableSeparator = $('<span />');
                syllableSeparator.addClass('syllable-separator');
                syllableSeparator.html('-');
                $this.before(syllableSeparator);
            }
        });
    }

    $('.page-title').click(function () {
        refreshLeftbarElementsHeight();
    })


    //  ambassadors functionality

    var ambassadors_list = $('.ambassadors-list');
    var maxRollupTileHeight = 0;
    var lastRolldownTile = null;

    function findMaxRollUpTileHeight() {
        var max_height = 0;
        var abassadors_tiles = $('.ambassador-tile');
        var rows = 0;
        abassadors_tiles.each(function () {
            var $this = $(this);
            var id = $this.attr('id');
            var nr = $this.data('nr');
            if (nr % 2 === 1) {
                rows++;
                $this.css('left', '0px');
            } else {
                $this.css('left', '380px');
            }
            if ($this.height() > max_height) {
                max_height = $this.height();
            }

        });
        max_height += 40;//padding top
        maxRollupTileHeight = max_height;
        abassadors_tiles.css('height', max_height + 'px');
        abassadors_tiles.removeClass('arrow-activator-rolldown');

        ambassadors_list.css('height', (4 * max_height) + 'px');

        rows = 0;
        abassadors_tiles.each(function () {
            var $this = $(this);
            var nr = $this.data('nr');
            $this.css('top', (rows * max_height) + 'px');
            if (nr % 2 === 0) {
                rows++;
            }
        });
    }

    function resetAmbassadorsListDimmension() {
        $('.ambassador-tile').css('height', 'auto');
        var longDescriptionContainer = $('.long-description');
        longDescriptionContainer.css('height', 'auto');
        longDescriptionContainer.css('height');
        longDescriptionContainer.css('height', '0px');
        findMaxRollUpTileHeight();
    }

    $('.photo, .arrow-activator').on('click', function () {
        var $this = $(this);
        var ambassadorTile = $this.parent().parent();
        var longDescriptionContainer = $('.long-description', ambassadorTile);
        var actualHeight = parseInt(longDescriptionContainer.css('height'), 10);
        var targetHeight = 0;
        $('.ambassador-tile').css('z-index', 1);
        if (actualHeight == 0) {//rolldown
            targetHeight = parseInt($('.hidden-content', longDescriptionContainer).css('height'), 10);
            if (lastRolldownTile != null) {
                lastRolldownTile
                    .css('height', maxRollupTileHeight + 'px')
                    .css('top', (parseInt(lastRolldownTile.data('row') - 1, 10) * maxRollupTileHeight) + 'px');
                $('.long-description', lastRolldownTile).css('height', '0px');
            }
            ambassadorTile.css('z-index', 3);
            lastRolldownTile = ambassadorTile;
        } else {
            targetHeight = 0;
        }
        if (lastRolldownTile != null) {
            lastRolldownTile.css('z-index', 2);
        }
        var minRolldownTileHeight = targetHeight + maxRollupTileHeight;
        var tileCorrectHeight = 1;
        while (minRolldownTileHeight > maxRollupTileHeight * tileCorrectHeight) {
            tileCorrectHeight += 1;
        }
        longDescriptionContainer
            .css('height', ((tileCorrectHeight - 1) * maxRollupTileHeight) + 'px');//height of long description

        var ambassadorTileRow = parseInt(ambassadorTile.attr('data-row'), 10);

        if (ambassadorTileRow + tileCorrectHeight > 5) {
            ambassadorTile.css('top', ((4 - (tileCorrectHeight - 1) - 1) * maxRollupTileHeight) + 'px');
        } else {
            ambassadorTile.css('top', ((ambassadorTileRow - 1) * maxRollupTileHeight) + 'px');
        }
        ambassadorTile.css(
            'height',
            (tileCorrectHeight * maxRollupTileHeight) + 'px'
        );

        ambassadorTile.toggleClass('arrow-activator-rolldown');
    });

    $(window).load(function () {
        findMaxRollUpTileHeight();
        $('.label').click(function () {
            resetAmbassadorsListDimmension();
        })
        $('.left-sidebar').addClass('sidebar-menu-transition-on');
    })

});
</script>
<?php
wp_footer();
?>
</body>
</html>

