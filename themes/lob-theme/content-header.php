<?php
global $page_style_info;
global $page_title;
?>
<div class="header <?php echo $page_style_info['header_scheme']; ?>">
    <div class="page-icon"><?php
        echo isset($page_style_info['page_icon']) ? '<img src="' .
            $page_style_info['page_icon'] . '" alt="" />' : '<span>' .
            $page_style_info['icon_text'] . '</span>';
        ?>
    </div>
    <div class="page-photo"><?php
        echo isset($page_style_info['page_thumbnail']) ? '<img src="' .
            $page_style_info['page_thumbnail'] . '" alt="" />' : '';
        ?>
    </div>
    <div class="title-container">
        <p class="page-title"><?php echo $page_title; ?></p>
    </div>
</div>