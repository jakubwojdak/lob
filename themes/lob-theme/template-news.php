<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 23.08.13
 * Time: 15:29
 * To change this template use File | Settings | File Templates.
 */
/*
 * Template Name: główna - Newsy
 */
session_start(); //to used to store redirect information
global $page_style_info;
$page_style_info = array();

$image_info = wp_get_attachment_image_src(
    get_post_thumbnail_id(get_post_id_of_template_filename('template-news')),
    'page-thumbnail'
);
if ($image_info && isset($image_info[0])) {
    $page_style_info['page_thumbnail'] = $image_info[0];
}

$page_style_info['page_icon'] = get_bloginfo('template_url') . '/images/news-icon.png';
$page_style_info['header_scheme'] = 'title-top style-news';

get_header();
?>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/pl_PL/all.js#xfbml=1&appId=155422744668174";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<?php
if ($_SESSION['redirect'] != null) {
    $redirect = $_SESSION['redirect'];
    unset($_SESSION['redirect']);
    ?>

<?php
}
$page_title = 'Newsy';
$block_name = 'news';
get_template_part('content', 'header');
?>
    <div class="content">
        <div class="left-sidebar style-news">
            <?php get_template_part('content', 'leftbar') ?>
        </div>

        <div class="main-container content-size-listener style-news">
            <?php
            if ($ob_content !== NULL) {
                echo $ob_content;
            } else {
                setup_postdata($post);
                $content = get_the_content();
                $galleries = get_and_strip_galleries($content);
                echo do_shortcode($content);
                //$content = apply_filters('the_content', $content);
                if (count($galleries)) {
                    foreach ($galleries as $gallery) {

                        $gallery = str_replace(
                            '[gallery',
                            '[gallery itemtag="li" icontag="span" size="gallery-image"',
                            $gallery
                        );
                        ?>

                        <div id="parent-slider" class="parent-slider-gallery">
                            <p class="gallery-header">galeria zdjęć</p>
                            <div class="navi-left"></div>
                            <div class="gallery-slider-container">
                                <?php
                                $html_gallery = preg_replace(
                                    "@<div id='gallery-@",
                                    "<ul id='gallery-",
                                    do_shortcode($gallery)
                                );

                                $html_gallery = str_replace('<br style="clear: both" />', '', $html_gallery);
                                $html_gallery = str_replace('<br style=\'clear: both;\' />', '', $html_gallery);
                                $attributes = "data-navi-class-left='navi-left' data-navi-class-right='navi-right' " .
                                    "class='gallery slide-auto gallery-slider ";

                                $html_gallery = str_replace("class='gallery ", $attributes, $html_gallery);
                                $html_gallery = str_replace('</div>', '</ul>', $html_gallery);

                                echo $html_gallery;
                                ?>
                            </div>
                            <div class="navi-right"></div>
                        </div>
                        <?php
                        break;
                    }
                }

            }

            get_template_part('content', 'attachements');
            ?>
            <div class="fb-comments" data-href="<?php echo get_permalink(); ?>"
                 data-numposts="5" data-width="700"></div>
        </div>
    </div>
    <script type="text/javascript">
        (jQuery)(function ($) {
            $('.left-sidebar a').on("click", function (e) {//to open baza wiedzy on blank page
                if (e.target.tagName == "A" && e.target.href == "<?php
            echo get_permalink(get_post_id_of_template_filename('template-knowledge-base.php'));
            ?>") {
                    e.target.setAttribute("target", "_blank");
                }
            });
        });
    </script>
<?php

get_footer();
