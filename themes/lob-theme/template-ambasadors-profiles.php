<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 29.08.13
 * Time: 16:43
 * To change this template use File | Settings | File Templates.
 */
/*
 * Template Name: Profile ambasadorów
 */

if (isset($_GET['reset'])) {
    $_GET['province'] = '';
    $_GET['y'] = null;
    $_GET['ambassador_name'] = '';
}

$years = array();
$last_year = $last_month = null;

$args = array(
    'post_type' => 'ambassador',
    'orderby' => 'date',
    'order' => 'DESC',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'suppress_filters' => true,
);

if ($_GET['province'] != '') {
    $args['tax_query'] = array(
        array(
            'taxonomy' => 'ambassador_province',
            'field' => 'slug',
            'terms' => $_GET['province']
        )
    );
}

$ambassadors = get_posts($args);
$ambassadors_by_criteries = array();

foreach ($ambassadors as $ambassador) {
    $date = strtotime($ambassador->post_date);
    $actual_year = date('Y', $date);
    $actual_month = date('m', $date);

    if ($last_year != $actual_year) {
        $years[] = $actual_year;
        $last_year = $actual_year;
    }

    $post_date = (int)date('Y', strtotime($ambassador->post_date));
    if ($_GET['y'] === null) {
        $_GET['y'] = $actual_year;
    }

    if ($_GET['y'] != '1') {
        if ($post_date > $_GET['y']) {
            continue;
        }
        if ($post_date < $_GET['y']) {
            continue;
        }
    }

    if ($_GET['ambassador_name'] != '') {
        if (strpos(strtolower($ambassador->post_title), strtolower($_GET['ambassador_name'])) === false) {
            continue;
        }
    }

    $ambassadors_by_criteries[] = $ambassador;
}

global $page_style_info;
$page_style_info = array();

$image_info = wp_get_attachment_image_src(
    get_post_thumbnail_id(get_post_id_of_template_filename('template-ambassadors-csr')),
    'page-thumbnail'
);
if ($image_info && isset($image_info[0])) {
    $page_style_info['page_thumbnail'] = $image_info[0];
}

$page_style_info['page_icon'] = get_bloginfo('template_url') . '/images/ambassadors-csr-icon.png';
$page_style_info['header_scheme'] = 'title-bottom style-ambassadors-csr';

get_header();

if ($_SESSION['redirect'] != null) {
    $redirect = $_SESSION['redirect'];
    unset($_SESSION['redirect']);
    ?>

<?php
}
$page_title = 'Ambasadorzy csr';
$block_name = 'ambassadors-csr';
get_template_part('content', 'header');
?>
    <div class="content">
        <div class="left-sidebar style-ambassadors-csr">
            <?php get_template_part('content', 'leftbar') ?>
        </div>

        <div class="main-container style-ambassadors-csr">

            <form class="search-ambassadors-form" action="<?php echo get_permalink($post->ID); ?>" method="get">
                <div class="ambassadors-search-container">
                    <div class="map-container">
                        <p class="label">Zaznacz obszar</p>

                        <p class="province">wszystkie</p>

                        <div id="map">
                            <div id="masks"></div>
                        </div>
                    </div>
                    <div class="search">
                        <p class="label">Wybierz rocznik ambasadora</p>
                        <input id="years" type="hidden" name="y" value=""/>

                        <div class="select-activator select-activator-year">
                            <span id="year-activator">wszystkie roczniki</span>

                            <div class="list">
                                <div class="elements-container">
                                    <span class="year-to-select" data-value="1">wszystkie roczniki</span>
                                    <?php
                                    foreach ($years as $year) {
                                        echo '<span class="year-to-select" data-value="' .
                                            $year . '">' . $year . ' / ' . ($year + 1) . '</span>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <p class="label">Nazwisko ambasadora</p>
                        <input placeholder="..." class="ambassador-name" name="ambassador_name" type="text"
                               value="<?php echo $_GET['ambassador_name']; ?>"/>
                    </div>
                </div>
                <input type="submit" name="reset" value="resetuj"/>
                <input type="submit" name="send" value="filtruj"/>
                <input id="province" type="hidden" name="province" value=""/>
            </form>
            <div class="ambassadors-list">
                <?php

                $all_posts = count($ambassadors_by_criteries);

                $posts_per_page = 8;
                $range = 3;
                $actual_page = 1;
                if ((int)$_GET['pg'] > 1) {
                    $actual_page = $_GET['pg'];
                }
                if ($all_posts) {
                    $posts_on_page = array();
                    for ($i = ($actual_page - 1) * $posts_per_page; $i < ($actual_page) * $posts_per_page; $i++) {
                        if (isset($ambassadors_by_criteries[$i])) {
                            $ambassador = $ambassadors_by_criteries[$i];

                            $iter++;
                            $photo = wp_get_attachment_image_src(
                                get_post_thumbnail_id($ambassador->ID),
                                'ambassador-photo'
                            );
                            $short_description = get_post_meta($ambassador->ID, 'short_description', true);
                            $email = get_post_meta($ambassador->ID, 'email', true);
                            ?>
                            <div data-nr="<?php echo $iter; ?>"
                                 data-row="<?php echo (int)(($iter - 1) / 2) + 1; ?>"
                                 id="ambassador-<?php echo $iter; ?>"
                                 class="ambassador-tile">
                                <div>
                                    <div class="photo">
                                        <?php
                                        if ($photo && isset($photo[0])) {
                                            echo '<img src="' . $photo[0] . '" alt="ambasador csr '
                                                . $ambassador->post_title . '" />';
                                        }
                                        ?>
                                    </div>
                                    <div class="short-description">
                                        <h3><?php echo $ambassador->post_title; ?></h3>
                                        <?php
                                        $terms = wp_get_post_terms($ambassador->ID, 'ambassador_status');
                                        if (count($terms)) {
                                            echo '<p class="status status-' . $terms[0]->slug .
                                                '">' . $terms[0]->name . '<p>';
                                        }
                                        ?>
                                        <p><?php echo $short_description; ?></p>
                                    </div>
                                    <div class="long-description">
                                        <div class="hidden-content">
                                            <?php
                                            if ($email != '') {
                                                echo '<p>Kontakt:<br /><a href="mailto:' .
                                                    $email . '">' . $ambassador->post_title . '</a></p>';
                                            }
                                            ?>
                                            <?php echo $ambassador->post_content; ?>
                                        </div>
                                    </div>
                                    <span class="arrow-activator"></span>
                                </div>
                            </div>
                        <?php
                        }
                    }
                }

                $this_link = get_permalink($post->ID);

                ?>
            </div>

            <div class="pagination-container">
                <span>strona</span>
                <?php
                echo get_pagination(
                    $this_link,
                    $all_posts,
                    $posts_per_page,
                    $range,
                    $actual_page,
                    ((int)$_GET['y'] > 0 ? '&amp;y=' . $_GET['y'] : '') .
                    ($_GET['ambassador_name'] != '' ? '&amp;ambassador_name=' . $_GET['ambassador_name'] : '') .
                    ($_GET['province'] > 0 ? '&amp;province=' . $_GET['province'] : '')
                );
                ?>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/Mapa/shapedetector_300_281.js"></script>

    <script type="text/javascript">
        jQuery(function ($) {
            var lastMaskName = null;

            var mouseMoveListener = function (currentMaskName) {
                var $masks = $('#masks');
                //$masks.css('cursor', 'default');
                $masks.removeClass(lastMaskName);
                if (currentMaskName != null) {
                    $masks.addClass(currentMaskName);
                    // $masks.css('cursor', 'pointer');
                }
                lastMaskName = currentMaskName;
            }
            
            var lastClickedMaskName = null;
            var mouseClickListener = function (maskName) {
                var province = null;
                var $map = $('#map');

                switch (maskName) {
                    case
                    'dolnoslaskie':
                        province = 'Dolnośląskie';
                        break;
                    case
                    'kujawsko-pomorskie':
                        province = 'Kujawsko-pomorskie';
                        break;
                    case
                    'lodzkie':
                        province = 'Łódzkie';
                        break;
                    case
                    'lubelskie':
                        province = 'Lubelskie';
                        break;
                    case
                    'lubuskie':
                        province = 'Lubuskie';
                        break;
                    case
                    'malopolskie':
                        province = 'Małopolskie';
                        break;
                    case
                    'mazowieckie':
                        province = 'Mazowieckie';
                        break;
                    case
                    'opolskie':
                        province = 'Opolskie';
                        break;
                    case
                    'podkarpackie':
                        province = 'Podkarpackie';
                        break;
                    case
                    'podlaskie':
                        province = 'Podlaskie';
                        break;
                    case
                    'pomorskie':
                        province = 'Pomorskie';
                        break;
                    case
                    'slaskie':
                        province = 'Śląskie';
                        break;
                    case
                    'swietokrzyskie':
                        province = 'Świętokrzyskie';
                        break;
                    case
                    'warminsko-mazurskie':
                        province = 'Warmińsko-mazurskie';
                        break;
                    case
                    'wielkopolskie':
                        province = 'Wielkopolskie';
                        break;
                    case
                    'zachodnio-pomorskie':
                        province = 'Zachodnio-pomorskie';
                        break;
                    default:
                        province = 'Wszystkie';
                }

                $('#province').val(maskName);
                if (maskName === null || lastClickedMaskName === maskName) {
                    maskName = 'mapa';
                    province = 'Wszystkie';
                    $('#province').val('');
                }
                $('.province').html(province);
                $map.css('background-image', 'url("<?php echo get_bloginfo('template_url'); ?>/images/mapa/' +
                    maskName
                    + '.png")');
                lastClickedMaskName = maskName;
            }

            <?php
                if($_GET['province'] != ''){
                    echo 'mouseClickListener("'.$_GET['province'].'");'."\n";
                }
                if($_GET['y'] != ''){
                    echo 'setSelectedYear($("[data-value='.$_GET['y'].']"));'."\n";
                }
            ?>

            Mapa.registerContainer('map');
            Mapa.addMouseMoveListener(mouseMoveListener);
            Mapa.addMouseClickListener(mouseClickListener);

            $('#year-activator').on('click', function () {
                var $this = $(this);

                var list = $('.list', $this.parent());
                if (list.height() == 0) {
                    list.css('height', $('.elements-container', list).css('height'));
                } else {
                    list.css('height', '0px');
                }
                $this.toggleClass('roll');
            });

            $('.year-to-select').on('click', function () {
                setSelectedYear($(this));
            });

            function setSelectedYear(yearComponent) {
                var yearActivator = $('#year-activator');
                yearActivator.html(yearComponent.html());
                $('#years').val(yearComponent.data('value'));
                yearComponent.parent().parent().css('height', '0px');
            }

        });
    </script>
<?php

get_footer();