<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 31.07.13
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
?>
<form class="searchform" action="/" method="get">
    <p>
        <input class="search-button" type="submit" value=""/>
        <input type="text" name="s" id="search" class="search-field" value="<?php the_search_query(); ?>"/>
    </p>
</form>