<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 23.08.13
 * Time: 15:40
 * To change this template use File | Settings | File Templates.
 */
global $parent_page_id;
global $block_ids;
$parent_page_id = $post->ID;

if ($parent_page_id) {
    $all_pages = get_posts(array(
        'post_type' => 'page',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'menu_order',
        'order' => 'ASC'
    ));

    $page_levels = array();
    $parent = get_post($block_ids['lob-for-students']);
    build_childs_tree($parent, $all_pages, $page_levels);

    echo '<ul class="sidebar-menu">' . "\n";
    echo '<li><span>LOB DLA STUDENTA</span>';
    get_childs_tree_list($page_levels[$block_ids['lob-for-students']], 'ul', 'li', $block_ids['lob-for-students']);
    echo '</li></ul>';
}
