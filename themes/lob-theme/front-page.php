<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 19.08.13
 * Time: 13:11
 * To change this template use File | Settings | File Templates.
 */
include(get_stylesheet_directory() . '/set-service-global-links.php');
get_header();
?>
    <div class="home-content">
        <div class="home-tiles">
            <div class="left-container">
                <a href="<?php
                echo get_permalink(get_first_child_page_id($block_ids['about-lob']));
                ?>" class="tile height2 width2 about-lob-tile"<?php
                $image_info = wp_get_attachment_image_src(
                    get_post_thumbnail_id($block_ids['about-lob']),
                    'home-double-xy'
                );

                if ($image_info && isset($image_info[0])) {
                    echo ' style="background-image:url(\'' . $image_info[0] . '\');"';
                }
                ?>>
                    <div>
                        <div class="roller">
                            <h2>Co to jest LOB ?</h2>

                            <div class="hidden-content">
                                <div class="container">
                                    <p>Liga Odpowiedzialnego Biznesu (LOB)
                                        to program edukacyjny dla studentów i studentek z całej Polski,
                                        kształcący z zakresu społecznej odpowiedzialności biznesu.
                                        Jest to program budujący przyszłe kadry menadżerów/ek
                                        oraz liderów/ek zmian społecznych </p>
                                    <span class="more">Dowiedz się wiecej &gt;&gt;&gt;</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>

                <div class="tile height2" style="position:relative;">
                    <a href="<?php echo get_permalink($block_ids['grant-program']); ?>" class="grant-program">
                        <div>
                            <div class="roller">
                                <h2>Program grantowy</h2>

                                <div class="hidden-content">
                                    <div class="container">
                                        <p class="syllable-container">Dedykowany organizacjom studenckim,
                                            które chcą promować CSR na swojej uczelni</p>
                                        <span class="more">Dowiedz się &gt;&gt;&gt;</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <a href="<?php
                $competition = get_page_by_title('Konkurs CSR');
                echo get_permalink($competition->ID);
                ?>" class="tile height1 width1 table competition-tile">
                    <div data-height-listeners-class="ambassadors-tile"
                         data-parent-class="competition-tile" class="roller">
                        <h2>Konkurs CSR</h2>

                        <div class="hidden-content" style="height: 0px;">
                            <div class="container">
                                <p>Ogólnopolski konkurs wiedzy o CSR dla studentów/ek oraz licealistów/ek.
                                    Do wygrania atrakcyjne nagrody!
                                </p>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?php
                echo get_permalink(get_first_child_page_id($block_ids['ambassadors-csr']));
                ?>" class="tile width2 height1 ambassadors-tile"<?php
                $image_info = wp_get_attachment_image_src(
                    get_post_thumbnail_id($block_ids['ambassadors-csr']),
                    'home-double-x'
                );
                if ($image_info && isset($image_info[0])) {
                    echo ' style="background-image:url(\'' . $image_info[0] . '\');"';
                }
                ?>>
                    <div>
                        <div data-parent-class="ambassadors-tile"
                             data-height-listeners-class="competition-tile,league-tile"
                             class="roller">
                            <h2>Ambasadorzy csr</h2>

                            <div class="hidden-content">
                                <div class="container">
                                    <p>Ogólnopolski program dla aktywnych studentów/ek
                                        chcących zdobywać wiedzę i promować CSR na swoich uczelniach
                                    </p>
                                    <span class="more">Dowiedz się więcej &gt;&gt;&gt;</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>

                <div class="tile width1 registry">
                    <?php
                    if ($block_ids['registration'] != null) {
                        echo '<p><a class="more" href="' . get_permalink($block_ids['registration']) .
                            '">Rejestracja</a></p>';
                    }
                    ?>
                </div>
                <a href="<?php
                $academy = get_page_by_title('Akademia Odpowiedzialnego Biznesu');
                echo get_permalink($academy->ID);
                ?>" class="tile width1 height1 academy-tile"<?php

                $image_info = wp_get_attachment_image_src(
                    get_post_thumbnail_id($academy->ID),
                    'page-thumbnail'
                );
                if ($image_info && isset($image_info[0])) {
                    echo ' style="background-image:url(\'' . $image_info[0] . '\');"';
                }
                ?>>
                    <div class="roller" data-parent-class="academy-tile"
                         data-height-listeners-class="news-tile">
                        <h2>Akademia Odpowiedzialnego Biznesu</h2>

                        <div class="hidden-content">
                            <div class="container">
                                <p>Organizowana od 2006 roku największa,
                                    ogólnopolska konferencja o CSR w Polsce dla studentów i studentek
                                </p>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?php
                echo get_permalink(get_first_child_page_id($block_ids['news']));

                ?>" class="tile width1 height1 news-tile"<?php
                $image_info = wp_get_attachment_image_src(
                    get_post_thumbnail_id($block_ids['news']),
                    'page-thumbnail'
                );
                if ($image_info && isset($image_info[0])) {
                    echo ' style="background-image:url(\'' . $image_info[0] . '\');"';
                }
                ?>>
                    <div class="roller" data-parent-class="news-tile"
                         data-height-listeners-class="academy-tile">
                        <h2>Newsy</h2>

                        <div class="hidden-content">
                            <div class="container">

                                <p class="syllable-container">
                                    Najświeższe informacje o działaniach w ramach Ligi Odpowiedzialnego Biznesu
                                </p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="right-container">
                <div class="title-box">
                    <h1>Odpowiedzialny biznes</h1>

                    <p>wspieraj promuj rozwijaj inspiruj</p>
                </div>
                <div class="tile width1 height2 turn-to-league-tile">
                    <div>
                        <h2>Liga Odpowiedzialnego Biznesu</h2>
                        <ul>
                            <li><span class="syllable-container">Jesteś odpowiedzialny?</span></li>
                            <li><span class="syllable-container">Przedsiębiorczy?</span></li>
                            <li><span>Chcesz nauczyć się, jak prowadzić odpowiedzialny biznes?</span></li>
                            <li><span>Chciałbyś wygrać staż?</span></li>
                        </ul>
                    </div>
                    <div class="container">
                        <a href="<?php
                        $league_page = get_page_by_title('Dołącz do nas');
                        echo get_permalink($league_page->ID);
                        ?>" class="more">Dołącz do nas &gt;&gt;&gt;</a>
                    </div>
                </div>
                <a href="<?php
                $csr = get_page_by_title('Podwieczorek przy CSR');
                echo get_permalink($csr->ID);
                ?>" class="tile width1 height1 csr-tile">
                    <div class="container">
                        <span class="more">Podwieczorek<br/>przy csr</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
<?php
$show_partners_slider_name = true;
get_template_part('content', 'partners-home');
?>
    <script type="text/javascript">
        (jQuery)(function ($) {
            $(document).ready(function () {
                $('.roller').on('hover', function () {//set height of hidden elements has required for transition
                    var $this = $(this);
                    var hiddenContent = $('.hidden-content', $this);
                    var $thisPaddingTB = parseInt($this.css('padding-top'), 10) + parseInt($this.css('padding-bottom'), 10);
                    var contentHeight = 0;
                    if (!$this.hasClass('roll-down')) {
                        //getting only in element is hover(not earlier, because is not synchronize with loading the webfont)
                        contentHeight = $('.container', hiddenContent).height();
                    }
                    hiddenContent.css('height', contentHeight + 'px');

                    var parentClass = $this.data('parent-class');
                    if (parentClass != null) {
                        var height = $('h2', $this).height();
                        var heightListeners = $this.data('height-listeners-class');
                        if (heightListeners != null) {
                            var listeners = heightListeners.split(',');
                        }
                        if (!$this.hasClass('roll-down') && height + contentHeight + $thisPaddingTB > 220) {
                            $('.' + parentClass).css('height', height + contentHeight + $thisPaddingTB);
                            if (listeners != null) {
                                for (var i = 0; i < listeners.length; i++) {
                                    var listener = $('.' + listeners[i]);
                                    listener.css('height', (height + contentHeight + $thisPaddingTB) + 'px');
                                }
                            }
                        } else {
                            $('.' + parentClass).css('height', '220px');
                            if (listeners != null) {
                                for (var i = 0; i < listeners.length; i++) {
                                    $('.' + listeners[i]).css('height', '220px');
                                }
                            }
                        }


                    }
                    $this.toggleClass('roll-down');
                });

                var specialSyllableWords = [];
                specialSyllableWords['Odpowiedzialnego'] = ['Odpo', 'wie', 'dzial', 'nego'];
                specialSyllableWords['odpowiedzialny?'] = ['odpo', 'wie', 'dzial', 'ny?'];
                specialSyllableWords['Przedsiębiorczy?'] = ['Przed', 'się', 'bior', 'czy?'];
                specialSyllableWords['ambasadorzy'] = ['amba', 'sa', 'do', 'rzy'];

                $('.syllable-container').each(function () {
                    var $this = $(this);
                    var textInContainer = $this.html();

                    for (var word in specialSyllableWords) {
                        var first = true;
                        var syllableWord = '';
                        for (var syllable in specialSyllableWords[word]) {

                            if (first) {
                                first = false;
                                syllableWord += specialSyllableWords[word][syllable];
                                continue;
                            }
                            syllableWord += '<span class="syllable">' + specialSyllableWords[word][syllable] + '</span>';
                        }
                        textInContainer = textInContainer.replace(word, syllableWord, 'g');
                        $this.html(textInContainer);
                    }
                });

                function refreshSyllableSepaprators() {
                    $('.syllable-container > .syllable-separator').remove();

                    $('.syllable').each(function () {
                        var $this = $(this);
                        var parent = $this.parent();
                        if ($this.offset().left - parent.offset().left == 0) {
                            var syllableSeparator = $('<span />');
                            syllableSeparator.addClass('syllable-separator');
                            syllableSeparator.html('-');
                            $this.before(syllableSeparator);
                        }
                    });
                }

                $('.title-box').click(function () {
                    refreshSyllableSepaprators();
                });
            });
        });
    </script>
<?php
get_footer();
