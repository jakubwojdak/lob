<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 16.07.13
 * Time: 15:54
 * To change this template use File | Settings | File Templates.
 */

function my_theme_setup()
{
    load_theme_textdomain('lob', get_template_directory() . '/languages');
}

add_action('after_setup_theme', 'my_theme_setup');


function initialize_logotomia_theme() //to add catalog to private materials
{
    $private_catalog = 'private';
    if (!file_exists("../wp-content/uploads/$private_catalog")) {
        mkdir("../wp-content/uploads/$private_catalog");
    }

    if (!file_exists("../wp-content/uploads/$private_catalog/.htaccess")) {
        if (!copy(
            get_stylesheet_directory() . "/autoload/htaccess",
            "../wp-content/uploads/$private_catalog/.htaccess"
        )
        ) {
            echo "failed to copy " . get_stylesheet_directory() . "/autoload/htaccess ...\n";
        } else {
            chmod("../wp-content/uploads/$private_catalog/.htaccess", 0644);
        }
    }

    if (!file_exists("../wp-content/uploads/$private_catalog/padlock.png")) {
        if (!copy(
            get_stylesheet_directory() . "/autoload/padlock.png",
            "../wp-content/uploads/$private_catalog/padlock.png"
        )
        ) {
            echo "failed to copy " . get_stylesheet_directory() . "/autoload/padlock.png ...\n";
        } else {
            chmod("../wp-content/uploads/$private_catalog/padlock.png", 0644);
        }
    }
}

add_action('after_switch_theme', 'initialize_logotomia_theme');

function get_first_child_page_id($parent_id)
{
    $lob_sub_pages = get_posts(
        array(
            'post_parent' => $parent_id,
            'post_type' => 'page',
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'posts_per_page' => 1
        )
    );

    if (count($lob_sub_pages)) {
        if ($lob_sub_pages[0]->ID > 0) {
            return get_first_child_page_id($lob_sub_pages[0]->ID);
        }
        return $lob_sub_pages[0]->ID;
    }
    return $parent_id;
}

function get_root_parent_template($root_parent_post)
{
    while ($root_parent_post->post_parent > 0) {
        $root_parent_post = get_post($root_parent_post->post_parent);
        if ($root_parent_post->post_parent === 0) {
            $parent_template = get_post_template_by_id($root_parent_post->ID);
            if ($parent_template != null) {
                return $parent_template;
            }
        }
    }
    return false;
}

function load_scripts_styles()
{
    wp_enqueue_script('auto-slider', get_stylesheet_directory_uri() .
        '/js/auto-slider.js', array('jquery'));
    wp_enqueue_script('jquery');
}

add_action('wp_enqueue_scripts', 'load_scripts_styles');

$imagesSize = array(
    'page-thumbnail' => array('width' => 220, 'height' => 220, 'crop' => true),
    'home-double-xy' => array('width' => 460, 'height' => 480, 'crop' => true),
    'home-double-x' => array('width' => 460, 'height' => 220, 'crop' => true),
    'partner-logo' => array('width' => 150, 'height' => 90, 'crop' => false),
    'ambassador-photo' => array('width' => 140, 'height' => 140, 'crop' => true),
    'gallery-image' => array('width' => 560, 'height' => 385, 'crop' => true)
);

add_theme_support('post-thumbnails', array('post', 'partner', 'page', 'ambassador'));

foreach ($imagesSize as $key => $options) {
    add_image_size($key, $options['width'], $options['height'], $options['crop']);
}

add_filter('use_default_gallery_style', '__return_false');


/*add_filter('get_image_tag', 'add_image_frames', 10, 2);
function add_image_frames($html)
{
    echo htmlspecialchars($html);
    return $html;
}*/

function register_my_menus()
{
    register_nav_menus(
        array(
            'partners_home' => __('Partners slider', 'lob'),
            'lang_menu' => __('Lang pages', 'lob'),
            'about_lob_footer' => __('About LOB - footer', 'lob'),
            'for_students_footer' => __('For students - footer', 'lob'),
            'ambassadors_footer' => __('Ambassadors - footer', 'lob')
        )
    );
}

add_action('init', 'register_my_menus');

function create_post_type()
{
    register_post_type('event',
        array(
            'labels' => array(
                'name' => __('Events', 'lob'),
                'all_items' => __('All', 'lob'),
                'add_new' => __('Add Event', 'lob'),
                'add_new_item' => __('Add new event', 'lob'),
                'singular_name' => 'event'
            ),
            'public' => true,
            'has_archive' => true,
            'menu_position' => 5,
            'supports' => array('title', 'custom-fields'),
            'rewrite' => array('slug' => __('event', 'lob'))
        )
    );

    register_post_type('partner',
        array(
            'labels' => array(
                'name' => __('Partners', 'lob'),
                'all_items' => __('All partners', 'lob'),
                'add_new' => __('Add partner', 'lob'),
                'add_new_item' => __('Add new partner', 'lob'),
                'singular_name' => 'partner'
            ),
            'public' => true,
            'has_archive' => true,
            'menu_position' => 5,
            'supports' => array('title', 'editor', 'custom-fields', 'thumbnail', 'page-attributes'),

            'rewrite' => array('slug' => __('partner', 'lob'))
        )
    );

    register_post_type('ambassador',
        array(
            'labels' => array(
                'name' => __('Ambassadors', 'lob'),
                'all_items' => __('All ambassadors', 'lob'),
                'add_new' => __('Add ambassador', 'lob'),
                'add_new_item' => __('Add new ambassador', 'lob'),
                'singular_name' => 'ambassador'
            ),
            'public' => true,
            'has_archive' => true,
            'menu_position' => 5,
            'supports' => array('title', 'editor', 'custom-fields', 'thumbnail'),
            'rewrite' => array('slug' => __('ambassador', 'lob'))
        )
    );
}


add_action('init', 'create_post_type');

function my_taxonomy_init()
{

    register_taxonomy(
        'partners_group',
        'partner',
        array(
            'label' => __('Partners Group', 'lob'),
            'rewrite' => array('with_front' => true, 'slug' => 'partners_group'),
            'hierarchical' => false
        )
    );

    register_taxonomy(
        'ambassador_status',
        'ambassador',
        array(
            'label' => __('Ambassador Status', 'lob'),
            'rewrite' => array('with_front' => true, 'slug' => 'ambassador_status'),
            'hierarchical' => false
        )
    );

    register_taxonomy(
        'ambassador_province',
        'ambassador',
        array(
            'label' => __('Ambassador Province', 'lob'),
            'rewrite' => array('with_front' => true, 'slug' => 'ambassador_province'),
            'hierarchical' => false
        )
    );
}

add_action('init', 'my_taxonomy_init');

function set_default_custom_fields($post_id)
{
    if ($_GET['post_type'] == 'ambassador') {

        if (get_post_meta($post_id, 'short_description', true) == '') {
            add_post_meta($post_id, 'short_description', '', true);
        }

        if (get_post_meta($post_id, 'email', true) == '') {
            add_post_meta($post_id, 'email', '', true);
        }

    } elseif ($_GET['post_type'] == 'partner') {

        if (get_post_meta($post_id, 'url', true) == '') {
            add_post_meta($post_id, 'url', '', true);
        }

    } elseif ($_GET['post_type'] == 'event') {

        if (get_post_meta($post_id, 'url', true) == '') {
            add_post_meta($post_id, 'url', '', true);
        }

    }
    return true;
}

add_action('wp_insert_post', 'set_default_custom_fields');

function get_blogs_ids()
{
    global $wpdb;
    $ids = array();
    $r = mysql_query('select blog_id from ' . $wpdb->prefix . 'blogs');
    if ($r && mysql_num_rows($r)) {
        while ($result = mysql_fetch_assoc($r)) {
            $ids[] = $result['blog_id'];
        }
    }
    return $ids;
}

function get_post_id_of_template_filename($template)
{
    global $wpdb;
    $response = null;
    if (!is_array($template)) {
        $s = 'select * from ' . $wpdb->postmeta .
            ' where meta_key = "_wp_page_template" AND (meta_value = "' . mysql_real_escape_string($template) . '"
        OR meta_value = "' . mysql_real_escape_string($template) . '.php") ORDER BY post_id ASC';
        $r = mysql_query($s);
        if ($r && mysql_num_rows($r)) {
            $tab = mysql_fetch_assoc($r);
            $response = $tab['post_id'];
        }
    } elseif (count($template)) {
        $template_name_id = array();
        foreach ($template as $template_name) {
            $template_name_id[$template_name] = null;
        }
        for ($i = 0; $i < count($template); $i++) {
            $template[$i] = mysql_real_escape_string($template[$i]);
        }
        $s = 'select post_id, meta_value from ' . $wpdb->postmeta .
            ' where meta_key = "_wp_page_template" AND (meta_value in ("' .
            implode('", "', $template) .
            '") OR meta_value in ("' . implode('.php", "', $template) .
            '.php")) ORDER BY post_id DESC';
        $r = mysql_query($s);
        if ($r && mysql_num_rows($r)) {
            while ($tab = mysql_fetch_assoc($r)) {
                if (in_array($tab['meta_value'], $template)) {
                    $template_name_id[$tab['meta_value']] = $tab['post_id'];
                } else {
                    $template_name_id[str_replace('.php', '', $tab['meta_value'])] = $tab['post_id'];
                }
            }
            $response = $template_name_id;
        }
    }

    return $response;
}

function get_post_template_by_id($post_id)
{
    global $wpdb;
    $s = 'select * from ' . $wpdb->prefix . 'postmeta
        where meta_key = "_wp_page_template" AND post_id = ' . (int)$post_id . ' ORDER BY post_id DESC';
    $r = mysql_query($s);
    $template = null;
    if ($r && mysql_num_rows($r)) {
        $tab = mysql_fetch_assoc($r);
        $template = $tab['meta_value'];
    }
    return $template;
}

function my_remove_menu_pages()
{
    remove_menu_page('tools.php');
    remove_menu_page('users.php');
    remove_menu_page('edit-comments.php');
    remove_menu_page('plugins.php');
    //remove_menu_page('themes.php');
    remove_menu_page('upload.php');
    remove_menu_page('edit.php');
}

//add_action( 'admin_menu', 'my_remove_menu_pages' );

add_action('after_switch_theme', 'initialize_theme');


function the_pagination($base_url, $all_posts_count, $posts_per_page, $range, $actual_page, $get_parameters = null)
{
    echo get_pagination($base_url, $all_posts_count, $posts_per_page, $range, $actual_page, $get_parameters);
}

function get_pagination($base_url, $all_posts_count, $posts_per_page, $range, $actual_page, $get_parameters = null)
{
    $parameters_url = $get_parameters;

    if (is_array($get_parameters)) {
        foreach ($get_parameters as $name_parameter => $value_parameter) {
            $parameters_url .= '&' . $name_parameter . '=' . $value_parameter;
        }
    }
    if (strpos($get_parameters, '&') !== 0 && strlen($get_parameters) > 0) {
        $get_parameters = '&' . $get_parameters;
    }
    $result = '<ul class="pagination">';

    $sep = '?';
    if (strpos($base_url, '?') !== false) {
        $sep = '&';
    }
    $all_pages = (int)($all_posts_count / $posts_per_page);
    if ($all_pages < $all_posts_count / $posts_per_page) {
        $all_pages += 1;
    }
    $start = $actual_page - ($range - 1); //first is added automatically
    $end = $actual_page + ($range - 1); //end is added automatically

    if ($start < 1) {
        $start = 1;
    }

    if ($actual_page <= $range) {
        $end += (($range - $actual_page) + 1);
    }
    if ($end > $all_pages) {
        $end = $all_pages;
    }

    if ($actual_page > 1) {
        $result .= '<li class="before"><a href="' .
            $base_url . $sep . 'pg=' . ($actual_page - 1) . $get_parameters . '">&lt;</a></li>';
    }

    if ($start > 1) {
        $result .= '<li><a href="' .
            $base_url . $sep . 'pg=1' . $get_parameters .
            '">1</a></li>';
        if ($start > 2) {
            $result .= '<li class="blank"><span>...</span></li>';
        }
    }

    for ($i = $start; $i <= $end; $i++) {
        $result .= '<li' . ($i == $actual_page ? ' class="active"' : '') . '><a href="' .
            $base_url . $sep . 'pg=' . $i . $get_parameters .
            '">' . $i . '</a></li>';
    }

    if ($end < $all_pages) {
        if ($end < $all_pages - 1) {
            $result .= '<li class="blank"><span>...</span></li>';
        }
        $result .= '<li' . ($i == $actual_page ? ' class="active"' : '') . '><a href="' .
            $base_url . $sep . 'pg=' . $all_pages . $get_parameters .
            '">' . $all_pages . '</a></li>';

    }
    if ($actual_page < $all_pages) {
        $result .= '<li class="next"><a href="' .
            $base_url . $sep . 'pg=' . ($actual_page + 1) . $get_parameters . '">&gt;</a></li>';
    }

    $result .= '</ul>';
    return $result;
}

function build_childs_tree($current_post, $all_pages_collection, &$page_by_levels, &$parent_of_current_id = 0)
{
    global $post;
    $page_by_levels[$current_post->ID]['childs'] = array();
    $page_by_levels[$current_post->ID]['post_data'] = $current_post;
    foreach ($all_pages_collection as $page) {
        if ($page->post_parent == $current_post->ID) {
            build_childs_tree(
                $page,
                $all_pages_collection,
                $page_by_levels[$current_post->ID]['childs'],
                $parent_of_current_id
            );
        }
    }
    if ($parent_of_current_id == $current_post->ID) {
        $page_by_levels[$current_post->ID]['status'] = 'parent-site';
        $parent_of_current_id = $current_post->post_parent;
    } elseif ($post->ID === $current_post->ID) {
        $parent_of_current_id = $current_post->post_parent;
        $page_by_levels[$current_post->ID]['status'] = 'current-site';
    }
}

function get_childs_tree_list($page_levels_child, $group_tag = 'ul', $element_tag = 'li', $dont_show_root = 0)
{
    if (isset($page_levels_child['post_data'])) {
        $class = $container_class = '';
        if (isset($page_levels_child['status'])) {
            $class = 'class="' . $page_levels_child['status'] . '" ';
            $container_class = ' ' . $page_levels_child['status'] . '-container';
        }
        if ($page_levels_child['post_data']->ID != $dont_show_root)
            echo '<' . $element_tag . '><a data-slug="' . $page_levels_child['post_data']->post_name . '" ' . $class .
                (
                count($page_levels_child['childs'])
                    ? 'data-container-id="' . $page_levels_child['post_data']->ID . '"'
                    : ''
                ) .
                ' href="' . get_permalink($page_levels_child['post_data']->ID) . '">' .
                $page_levels_child['post_data']->post_title . '</a>' . "\n";
        if (count($page_levels_child['childs'])) {
            echo '<' . $group_tag . ' class="container-' . $page_levels_child['post_data']->ID .
                $container_class . ' ' . $page_levels_child['post_data']->post_name . '-container"' . '>' . "\n";
            foreach ($page_levels_child['childs'] as $child) {
                get_childs_tree_list($child);
            }
            echo '</' . $group_tag . '>' . "\n";
        }
        if ($page_levels_child['post_data']->ID != $dont_show_root)
            echo '</' . $element_tag . '>' . "\n";
    }
}

function edit_gallery_shortcodes($content)
{
    $gallery_shortcodes = array();
    $galleries = array();
    //preg_match_all('@\[gallery[^\]]*\]@', $content, $galleries);

    $content = str_replace('[gallery', '[gallery itemtag="li" icontag="span" size="gallery-image"', $content);

    /* foreach ($galleries[0] as $gallery) {
         $gallery_shortcodes[] = $gallery;
         //echo gallery;

     }*/
    return $content;
}

//add_filter('post_gallery', 'flipboard_gallery_shortcode', 10, 2);

add_filter('the_content', 'edit_gallery_shortcodes', 10);


function get_and_strip_galleries(&$content)
{
    $gallery_shortcodes = array();
    $galleries = array();
    preg_match_all('@\[gallery[^\]]*\]@', $content, $galleries);
    foreach ($galleries[0] as $gallery) {
        $gallery_shortcodes[] = $gallery;
        $content = str_replace($gallery, '', $content);
    }
    return $gallery_shortcodes;
}

//add_shortcode( 'gallery', 'file_gallery_shortcode' );

function file_gallery_shortcode($atts)
{
    $attr['size'] = 'partner-logo';
    return gallery_shortcode($atts);
}

function custom_password_form($content)
{
    $before = array('Ten wpis jest zabezpieczony hasłem. Aby go zobaczyć, proszę wprowadzić swoje hasło poniżej:', 'Hasło:', 'Wyślij');
    $after = array('Aby uzyskać dostęp do materiałów wpisz hasło:', '', 'OK');
    $content = str_replace($before, $after, $content);
    return $content;
}

add_filter('the_password_form', 'custom_password_form');

function custom_upload_directory($args)
{
    $id = $_REQUEST['post_id'];
    $parent = get_post($id)->post_parent;
    $private_materials_post_id = get_post_id_of_template_filename('template-materials');
    if ($private_materials_post_id == $parent || $private_materials_post_id == $id) {
        $args['path'] = $args['basedir'] . '/private';
        $args['url'] = $args['baseurl'] . '/private';
        $args['subdir'] = '/private';
    }
    return $args;
}

add_filter('upload_dir', 'custom_upload_directory');