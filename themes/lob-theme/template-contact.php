<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 02.09.13
 * Time: 15:25
 * To change this template use File | Settings | File Templates.
 */
/*
 * Template Name: Kontakt
 */
global $page_style_info;
$page_style_info = array();

$image_info = wp_get_attachment_image_src(
    get_post_thumbnail_id($post->ID),
    'page-thumbnail'
);
if ($image_info && isset($image_info[0])) {
    $page_style_info['page_thumbnail'] = $image_info[0];
}

$page_style_info['page_icon'] = get_bloginfo('template_url') . '/images/contact-icon.png';
$page_style_info['header_scheme'] = 'title-top style-full-width-page';

get_header();

$page_title = $post->post_title;
get_template_part('content', 'header');
?>
    <div class="content">
        <div class="main-container content-size-listener full-width-container style-full-width-page">
            <?php
            setup_postdata($post);
            $content = get_the_content();

            echo $content;
            //$content = apply_filters('the_content', $content);
            get_template_part('content', 'attachements');

            ?>
        </div>
    </div>
<?php

get_footer();