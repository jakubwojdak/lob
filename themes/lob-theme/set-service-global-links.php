<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 29.07.13
 * Time: 17:40
 * To change this template use File | Settings | File Templates.
 */

$templates_ids = get_post_id_of_template_filename(
    array(
        'template-about-lob',
        'template-lob-for-students',
        'template-news',
        'template-csr',
        'template-grant-program',
        'template-registration',
        'template-materials',
        'template-ambassadors-csr',
        'template-contact',
        'template-join-the-league'
    )
);

global $block_ids;
$block_ids['about-lob'] = $templates_ids['template-about-lob'];
$block_ids['lob-for-students'] = $templates_ids['template-lob-for-students'];
$block_ids['news'] = $templates_ids['template-news'];
$block_ids['csr'] = $templates_ids['template-csr'];
$block_ids['grant-program'] = $templates_ids['template-grant-program'];
$block_ids['registration'] = $templates_ids['template-registration'];
$block_ids['materials'] = $templates_ids['template-materials'];
$block_ids['join-the-league'] = $templates_ids['template-join-the-league'];
$block_ids['contact'] = $templates_ids['template-contact'];
$block_ids['ambassadors-csr'] = $templates_ids['template-ambassadors-csr'];
