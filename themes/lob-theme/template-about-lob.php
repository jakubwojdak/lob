<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 12.08.13
 * Time: 11:54
 * To change this template use File | Settings | File Templates.
 */
/*
 * Template Name: główna - O LOB
 */
session_start(); //to used to store redirect information
global $page_style_info;
$page_style_info = array();

$image_info = wp_get_attachment_image_src(
    get_post_thumbnail_id(get_post_id_of_template_filename('template-about-lob')),
    'page-thumbnail'
);

if ($image_info && isset($image_info[0])) {
    $page_style_info['page_thumbnail'] = $image_info[0];
}

$page_style_info['page_icon'] = get_bloginfo('template_url') . '/images/about-lob-icon.png';
$page_style_info['header_scheme'] = 'title-top';

get_header();

if ($_SESSION['redirect'] != null) {
    $redirect = $_SESSION['redirect'];
    unset($_SESSION['redirect']);
    ?>

<?php
}
$page_title = 'O LOB';
get_template_part('content', 'header');
?>
    <div class="content">
        <div class="left-sidebar style-about-lob">
            <?php get_template_part('content', 'about-lob-leftbar') ?>
        </div>

        <div class="main-container content-size-listener style-about-lob">
            <?php
            if ($ob_content !== NULL) {
                echo $ob_content;
            } else {
                echo wpautop($post->post_content);
            }

            get_template_part('content', 'attachements');
            ?>
        </div>
    </div>
    <script type="text/javascript">
        (jQuery)(function ($) {
            $('.left-sidebar a').on("click", function (e) {//to open baza wiedzy on blank page
                if (e.target.tagName == "A" && e.target.href == "<?php
            echo get_permalink(get_post_id_of_template_filename('template-knowledge-base.php'));
            ?>") {
                    e.target.setAttribute("target", "_blank");
                }
            });
        });
    </script>
<?php

get_footer();
