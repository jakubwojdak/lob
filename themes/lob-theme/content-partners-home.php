<?php
global $show_partners_slider_name;
$locations = get_nav_menu_locations();
$lang_pages = array();
if (isset($locations['partners_home'])) :
    $menu_items = wp_get_nav_menu_items($locations['partners_home']);
    if ($menu_items) {
        ?>
        <div id="parent-slider"
             class="parent-slider-partners <?php echo $show_partners_slider_name ? 'slider-top-margin' : ''; ?>">
            <?php
            if ($show_partners_slider_name) {
                echo '<p class="partners-title">PARTNERZY</p>';
            }
            ?>
            <div class="navi-left"></div>
            <div class="slider-container partners-slider-container">

                <ul class="slide-auto partners-slider" data-navi-class-left="navi-left"
                    data-show-at-a-time="4"
                    data-navi-class-right="navi-right">
                    <?php
                    foreach ($menu_items as $menu_item) {
                        $partner = get_post($menu_item->object_id);
                        $link = get_post_meta($partner->ID, 'url', true);
                        $image_info = wp_get_attachment_image_src(get_post_thumbnail_id($partner->ID), 'partner-logo');
                        if ($image_info[0] != '') {
                            $html = '<img src="' . $image_info[0] . '" alt="' . $partner->post_title . '" />';
                            if ($link != '') {
                                $html = '<a target="_blank" href="' . $link . '">' . $html . '</a>';
                            } else {
                                $html = '<span>' . $html . '</span>';
                            }
                            echo '<li class="slide-element">' . $html . '</li>';
                        }
                    }
                    ?>
                </ul>
            </div>
            <div class="navi-right"></div>
        </div>
    <?php
    }
endif;
