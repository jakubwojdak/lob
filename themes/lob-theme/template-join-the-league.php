<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 12.08.13
 * Time: 13:59
 * To change this template use File | Settings | File Templates.
 */

/*
* Template Name: Włącz się do ligi
*/
global $page_style_info;
$page_style_info = array();

$image_info = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'page-thumbnail');
if ($image_info && isset($image_info[0])) {
    $page_style_info['page_thumbnail'] = $image_info[0];
}

$page_style_info['page_icon'] = get_bloginfo('template_url') . '/images/about-lob-icon.png';
$page_style_info['header_scheme'] = 'title-top';

get_header();

get_template_part('content', 'header');
?>
    <div class="content">
        <div class="left-sidebar">
            <h2><?php echo $post->post_title; ?></h2>
            <ul class="sidebar-menu">
                <li><a href="">profile ambasadorów</a>
                    <ul>
                        <li></li>
                    </ul>
                </li>
                <li><a href="">rekrutacja</a></li>
            </ul>
        </div>

        <div class="main-container content-size-listener">
            <p>to jest jakis tekst</p>

            <div>
                <p>to jest jakis tekst w divie</p>
            </div>
        </div>
    </div>

<?php
get_footer();