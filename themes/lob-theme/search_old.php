<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

function linkText($matches)
{
    return $matches[1];
}

function removeLinks(&$content)
{
    $content = preg_replace_callback('@<a [^>]+>([^<]*)<\/a>@', 'linkText', $content);
}

function removeImages(&$content)
{
    $content = preg_replace('@<img [^>]+>@i', '', $content);
}

function removeShortcodes(&$content)
{
    $content = preg_replace('@\[[^\]]+\]@i', '', $content);
}

function removeOpenTags(&$content)
{
    $content = preg_replace('@<[^>]+>@i', '', $content);
}

function removeCloseTags(&$content)
{
    $content = preg_replace('@<[^>]+\/>@i', '', $content);
}

get_header();

global $page_style_info;
$page_style_info = array();


$page_style_info['page_thumbnail'] = get_bloginfo('template_url') . '/images/search-page-header-image.png';
$page_style_info['page_icon'] = get_bloginfo('template_url') . '/images/search-icon.png';
$page_style_info['header_scheme'] = 'title-top search-result-page-title';

$page_title = 'Wyniki wyszukiwania';
get_template_part('content', 'header');
?>
    <div class="content">
        <div class="main-container full-width-container">
            <p class="search-title">Wyniki wyszukiwania dla frazy:</p>

            <p class="search-word"><?php echo $_GET['s']; ?></p>

            <div class="search-results-page">
                <?php

                $searched_posts = array();

                if (isset($_GET['s']) && strlen($_GET['s']) > 2) {
                    $args = array(
                        'post_type' => array('page', 'post', 'ambassador'),
                        'posts_per_page' => -1,
                        's' => $_GET['s'],
                    );

                    $wp_query = new WP_Query($args);

                    $r = mysql_query($s = 'select post_id, meta_value from ' .
                        $wpdb->prefix .
                        'postmeta where (meta_key = "subtitle" OR meta_key = "subdescription" OR meta_key = "lead" OR  meta_key = "lead_home" OR meta_key = "title_home") AND meta_value LIKE "%' .
                        mysql_real_escape_string($_GET['s']) . '%" GROUP BY post_id');

                    if ($r && mysql_num_rows($r)) {
                        while ($tab = mysql_fetch_assoc($r)) {
                            $searched_posts[$tab['post_id']]['post'] = get_post($tab['post_id']);
                            $searched_posts[$tab['post_id']]['content'] = $tab['meta_value'];
                        }
                    }
                    if ($wp_query->have_posts()) {

                        while ($wp_query->have_posts()) : $wp_query->the_post();
                            $searched_posts[$post->ID]['post'] = $post;
                            $searched_posts[$post->ID]['content'] = $post->post_content;
                        endwhile;
                    }
                    $iter = 0;
                    if (count($searched_posts)) {

                        foreach ($searched_posts as $id => $data_post) {
                            $post = $data_post['post'];

                            echo '<div class="result">';

                            echo '<h1 class="name"><a href="' . get_permalink($post->ID) . '">' .
                                str_ireplace(
                                    $_GET['s'],
                                    '<span class="select">' . $_GET['s'] . '</span>',
                                    $post->post_title
                                ) . '</a></h1>';

                            $content = $data_post['content'];

                            removeImages($content);
                            removeLinks($content);

                            removeShortcodes($content);

                            removeOpenTags($content);

                            removeCloseTags($content);

                            $content = str_ireplace($_GET['s'], '&finded&', $content, $count);

                            $all_words_array = explode(' ', $content);

                            $result_words_array = array();
                            $finded_index = 0;
                            $after = $before = 20;
                            foreach ($all_words_array as $index => $word) {
                                if (false !== strpos(strtolower($word), '&finded&')) {
                                    $finded_index = $index;
                                    break;
                                }
                            }

                            if ($finded_index - $before < 0) {
                                $after += $before - $finded_index;
                                $before = $finded_index;
                            } else if ($finded_index + $after > count($all_words_array) - 1) {
                                $before += $finded_index + $after - (count($all_words_array) - 1);
                                if ($finded_index - $before < 0)
                                    $before = $finded_index;
                            }

                            if ($finded_index - $before > 0)
                                $result_words_array[] = '...';
                            for ($i = $finded_index - $before; $i <= $finded_index + $after; $i++) {
                                if (isset($all_words_array[$i])) {
                                    $w = $all_words_array[$i];
                                    if (false !== strpos($w, '&finded&')) {
                                        $w = str_replace('&finded&', '<strong>' . $_GET['s'] . '</strong>', $w);
                                    }
                                    $result_words_array[] = $w;
                                } else {
                                    break;
                                }

                            }
                            if ($finded_index + $after < count($all_words_array))
                                $result_words_array[] = '...';


                            echo '<div class="text">' . implode(' ', $result_words_array) . '</div>';

                            echo '</div>'; //end .result
                            if (++$iter > 14) {
                                break;
                            }
                        }
                        //end foreach
                    } else {
                        echo '<p class="message">Nie znaleziono żadnych wyników wyszukiwania</p>';
                    }
                } else {
                    echo '<p class="message">Prosze wprowadzić więcej znaków w polu wyszukiwania</p>';
                }
                ?>
            </div>
        </div>
    </div>
    </div>
<?php
get_footer();
