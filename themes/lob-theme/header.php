<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 12.08.13
 * Time: 12:40
 * To change this template use File | Settings | File Templates.
 */
include(get_stylesheet_directory() . '/set-service-global-links.php');
global $page_style_info;
?><!DOCTYPE HTML>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=<?php bloginfo('charset'); ?>"/>
    <title><?php
        echo is_front_page() ? get_bloginfo('name') : get_bloginfo('name') . ' - ' . wp_title('', false, 'right');
        ?></title>
    <?php wp_head(); ?>
    <link rel="shortcut icon" href="<?php echo get_bloginfo('template_url') . '/images/favicon.jpg'; ?>"/>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>"/>
    <link rel="stylesheet" type="text/css" href="//cloud.typography.com/7290052/732282/css/fonts.css" />
</head>
<body <?php body_class(); ?>>
<div class="cookie-info-container">
    <?php
    if (!$_COOKIE['cookie_accept']) {
        ?>
        <div class="cookie-info">Uwaga! Ta strona wykorzystuje pliki cookies, musisz zakceptować przyjmowanie ciasteczek
            jeśli chesz korzystać z pełnej funkcjonalności serwisu <span class="cookie-accept">&lt;&lt; akceptuję &gt;&gt;</span>
        </div>
        <script type="text/javascript">
            (jQuery)(function ($) {
                $('.cookie-accept').on('click', function () {
                    var date = new Date();
                    date.setFullYear(date.getFullYear() + 1);
                    document.cookie = "cookie_accept=true;expires=" + date.toUTCString() + ";path=/;";
                    $('.cookie-info').css('display', 'none');
                });
            });
        </script>
    <? } ?>
    <div class="search-container">
        <?php get_search_form(); ?>
        <span class="bw-box"></span>

        <div class="font-size-toggler"><span class="root-font higher"> + </span> <span
                class="root-font normal"> A </span> <span
                class="root-font smaller"> - </span>
        </div>
        <?php
        $locations = get_nav_menu_locations();
        $lang_pages = array();
        if (isset($locations['lang_menu'])) {
            $menu_items = wp_get_nav_menu_items($locations['lang_menu']);
            if ($menu_items) {
                foreach ($menu_items as $menu_item) {
                    $lang_page = get_post($menu_item->object_id);
                    $lang = get_post_meta($lang_page->ID, 'icon_text', true);
                    if ($lang == $page_style_info['icon_text']) {
                        $lang_pages[] = array(
                            'permalink' => get_bloginfo('wpurl'),
                            'title' => 'PL'
                        );
                    } else {
                        $lang_pages[] = array(
                            'permalink' => get_permalink($lang_page->ID),
                            'title' => $lang
                        );
                    }

                }
            }
        }
        if (count($lang_pages)) {
            foreach ($lang_pages as $lang_page) {
                echo '<span class="root-font lang"><a href="' . $lang_page['permalink'] . '">' .
                    $lang_page['title'] . '</a></span>';
            }
        }
        ?>
    </div>
</div>
<div class="page-container">
    <a class="logo" href="/"><img
            src="<?php echo get_bloginfo('template_url') . '/images/liga-odpowiedzialnego-biznesu.png'; ?>"
            alt="Liga Odpowiedzialnego Biznesu"/></a>

