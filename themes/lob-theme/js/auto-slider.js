(jQuery)(function ($) {
    $(document).ready(function () {

        var clockTogglers = [];

        function startClock(elementId, interval) {
            clockTogglers[elementId] = self.setInterval(function () {
                showNext(elementId)
            }, interval);
        }

        function stopClock(elementId) {
            window.clearInterval(clockTogglers[elementId]);
        }

        function addAdditionalSlides(slider, showAtATime){
            var slides = $('li', slider);

            if (slides.length > 1) {
                var newSlideCount = showAtATime;
                slides.each(function () {
                    if ((newSlideCount--) > 0) {
                        var $this = $(this);
                        slider.append($this.clone());
                    }
                });
            }
        }

        function showNext(elementId) {
            var $this = $('#' + elementId);
            var all_width = parseInt($this.css('width'));

            var firstShowingNr = $this.data('first-showed');
            if (firstShowingNr == null) {
                firstShowingNr = 1;
            }

            var slideOn = parseInt($('.slide-element, .gallery-item', $this).first().css('width'));
            var nextLeftPosition = firstShowingNr * slideOn;
            if (all_width > (nextLeftPosition + ($this.data('show-at-a-time')-1) * slideOn)) {
                $this.css('left', '-' + (nextLeftPosition) + 'px');
                $this.data('first-showed', firstShowingNr + 1);
            } else {
                $this.addClass('no-transition');
                $this.css('left', '0px');
                $this.css('left');
                $this.removeClass('no-transition');
                $this.css('left', '-' + slideOn + 'px');
                $this.data('first-showed', 2);
            }
        }

        function showPrevious(elementId) {
            var $this = $('#' + elementId);
            var all_width = parseInt($this.css('width'));

            var firstShowingNr = $this.data('first-showed');
            if (firstShowingNr == null) {
                firstShowingNr = 1;
            }

            var slideOn = parseInt($('.slide-element, .gallery-item', $this).first().css('width'));
            var nextLeftPosition = (firstShowingNr - 2) * slideOn;
            if (nextLeftPosition >= 0) {
                $this.css('left', '-' + nextLeftPosition + 'px');
                $this.data('first-showed', firstShowingNr - 1);
            } else {//start position
                $this.addClass('no-transition');
                $this.css('left', '-' + (all_width - $this.data('show-at-a-time') * slideOn) + 'px');
                $this.css('left');
                $this.removeClass('no-transition');
                $this.css('left', '-' + (all_width - ($this.data('show-at-a-time')+1) * slideOn) + 'px');
                $this.data('first-showed', (all_width / slideOn) - $this.data('show-at-a-time'));
            }
        }

        $('.slide-auto').each(function () {
            var $this = $(this);

            var showAtATime = $this.data('show-at-a-time');
            if (showAtATime == null) {
                showAtATime = 1;
                $this.data('show-at-a-time', showAtATime);
            }
            addAdditionalSlides($this, showAtATime);

            var secondsDelay = $this.data('slide-auto-interval');
            if (secondsDelay == null) {
                secondsDelay = 4;
            }

            var slideInterval = secondsDelay * 1000;
            var thisId = $this.attr('id');
            if (thisId == null) {
                thisId = 'slider1';
                $this.attr('id', thisId);
            }
            var parent = $this.parent();
            var parentWidth = parent.css('width');

            parent.css('position', 'relative');

            var slideElements = $('.slide-element, .gallery-item', $this);

            $this.css('width', parseInt(parentWidth) / showAtATime * (slideElements.length));
            $this.css('position', 'absolute');
            $this.css('left', '0px');

            slideElements.css('width', (parseInt(parentWidth) / showAtATime) + 'px');

            if (slideElements.length > 1) {
                var naviClassLeft = $this.data('navi-class-left');

                var naviClassRight = $this.data('navi-class-right');
                if (naviClassLeft != null) {
                    var naviLeft = $('.' + naviClassLeft);

                    if (naviLeft[0] != null) {
                        naviLeft.on('click', function () {
                            showPrevious(thisId)
                        });
                    }
                }
                if (naviClassRight != null) {
                    var naviRight = $('.' + naviClassRight);
                    if (naviRight[0] != null) {
                        naviRight.on('click', function () {
                            showNext(thisId)
                        });
                    }
                }

                $('#parent-slider').mouseenter(function () {
                    stopClock(thisId)
                });
                $('#parent-slider').mouseleave(function () {
                    startClock(thisId, slideInterval)
                });

                startClock(thisId, slideInterval);
            }

        });
    });
});