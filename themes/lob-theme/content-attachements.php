<?php
$args = array(
    'post_type' => 'attachment',
    'numberposts' => -1,
    'post_status' => 'any',
    'post_parent' => $post->ID,
    'post_mime_type' => 'application'
);

$attachments = get_posts($args);
if ($attachments) {
    echo '<div class="attachments"><p><strong>Materiały do pobrania:</strong></p>';
    foreach ($attachments as $attachment) {
        $url = wp_get_attachment_url($attachment->ID);
        $title = apply_filters('the_title', $attachment->post_title);
        ?>
            <?php
            echo '<p class="attachment"> - <a href="' . $url . '" class="' . $attachment->post_mime_type . '">' .
                ($attachment->post_excerpt != '' ? $attachment->post_excerpt : $title) .
                '</a></p>';
            ?>
        <?php

    }
    echo '</div>';
}
