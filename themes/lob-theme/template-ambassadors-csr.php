<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 28.08.13
 * Time: 11:06
 * To change this template use File | Settings | File Templates.
 */
/*
 * Template Name: główna - Ambasadorzy CSR
 */
global $page_style_info;
$page_style_info = array();

$image_info = wp_get_attachment_image_src(
    get_post_thumbnail_id(get_post_id_of_template_filename('template-ambassadors-csr')),
    'page-thumbnail'
);
if ($image_info && isset($image_info[0])) {
    $page_style_info['page_thumbnail'] = $image_info[0];
}

$page_style_info['page_icon'] = get_bloginfo('template_url') . '/images/ambassadors-csr-icon.png';
$page_style_info['header_scheme'] = 'title-bottom style-ambassadors-csr';

get_header();

if ($_SESSION['redirect'] != null) {
    $redirect = $_SESSION['redirect'];
    unset($_SESSION['redirect']);
    ?>

<?php
}
$page_title = 'Ambasadorzy csr';
$block_name = 'ambassadors-csr';
get_template_part('content', 'header');
?>
    <div class="content">
        <div class="left-sidebar style-ambassadors-csr">
            <?php get_template_part('content', 'leftbar') ?>
        </div>

        <div class="main-container style-ambassadors-csr">
            <?php
            if ($ob_content !== NULL) {
                echo $ob_content;
            } else {
                setup_postdata($post);
                $content = get_the_content();

                echo $content;
                //$content = apply_filters('the_content', $content);
                get_template_part('content', 'attachements');
            }
            ?>
        </div>
    </div>
    <script type="text/javascript">
        (jQuery)(function ($) {
            $('.left-sidebar a').on("click", function (e) {//to open baza wiedzy on blank page
                if (e.target.tagName == "A" && e.target.href == "<?php
            echo get_permalink(get_post_id_of_template_filename('template-knowledge-base.php'));
            ?>") {
                    e.target.setAttribute("target", "_blank");
                }
            });
        });
    </script>
<?php

get_footer();