<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header();

global $page_style_info;
$page_style_info = array();


$page_style_info['page_thumbnail'] = get_bloginfo('template_url') . '/images/search-page-header-image.png';
$page_style_info['page_icon'] = get_bloginfo('template_url') . '/images/search-icon.png';
$page_style_info['header_scheme'] = 'title-top search-result-page-title';

$page_title = 'Wyniki wyszukiwania';
get_template_part('content', 'header');

global $query_string;

$args = explode("&", $query_string);
$search_query = array();

foreach ($args as $key => $string) {
    $query_split = explode("=", $string);
    $search_query[$query_split[0]] = urldecode($query_split[1]);
}
?>
    <div class="content">
        <div class="main-container full-width-container">
            <p class="search-title">Wyniki wyszukiwania dla frazy:</p>

            <p class="search-word"><?php echo $search_query['s']; ?></p>

            <div class="search-results-page">
                <?php

                $searched_posts = array();

                if (isset($search_query['s']) && strlen($search_query['s']) > 2) {
                    $args = array(
                        'post_type' => array('page', 'post', 'ambassador'),
                        'posts_per_page' => -1,
                        'post_status' => 'publish'
                    );

                    $wp_query = new WP_Query($search_query);

                    $r = mysql_query($s = 'select post_id, meta_value from ' .
                        $wpdb->prefix .
                        'postmeta where (meta_key = "subtitle" OR meta_key = "subdescription" OR meta_key = "lead" OR  meta_key = "lead_home" OR meta_key = "title_home") AND meta_value LIKE "%' .
                        mysql_real_escape_string($search_query['s']) . '%" GROUP BY post_id');

                    if ($r && mysql_num_rows($r)) {
                        while ($tab = mysql_fetch_assoc($r)) {
                            $searched_posts[$tab['post_id']]['post'] = get_post($tab['post_id']);
                            $searched_posts[$tab['post_id']]['content'] = $tab['meta_value'];
                        }
                    }

                    if ($wp_query->have_posts()) {
                        while ($wp_query->have_posts()) : $wp_query->the_post();
                            $searched_posts[$post->ID]['post'] = $post;
                            $searched_posts[$post->ID]['content'] = $post->post_content;
                        endwhile;
                    }

                    if(function_exists('search_results_filter_plugin_filter_post_content')){
                        add_filter('the_title', 'search_results_filter_plugin_filter_post_content', 10, 6);
                    }
                    if(function_exists('search_results_filter_plugin_filter_post_content')){
                        add_filter('the_content', 'search_results_filter_plugin_filter_post_content', 10, 6);
                    }

                    $iter = 0;
                    if (count($searched_posts)) {

                        foreach ($searched_posts as $id => $data_post) {
                            $post = $data_post['post'];
                            setup_postdata($post);

                            echo '<div class="result">';

                            echo '<h1 class="name"><a href="' . get_permalink($post->ID) . '">' .
                                apply_filters('the_title', get_the_title(), 5, true, true, 'span', 'select')
                                . '</a></h1>';

                            echo '<div class="text">' . apply_filters('the_content', get_the_content() , 16, true, true, 'strong', '') . '</div>';

                            echo '</div>'; //end .result
                        }
                        //end foreach
                    } else {
                        echo '<p class="message">Nie znaleziono żadnych wyników wyszukiwania</p>';
                    }

                    remove_filter('the_title', 'search_filter_plugin_filter_post_title');
                    remove_filter('the_content', 'search_filter_plugin_filter_post_content');

                } else {
                    echo '<p class="message">Prosze wprowadzić więcej znaków w polu wyszukiwania</p>';
                }
                ?>
            </div>
        </div>
    </div>
    </div>
<?php
get_footer();
