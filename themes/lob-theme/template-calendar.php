<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 22.08.13
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
/*
 * Template Name: O LOB - Kalendarz
 */
$sorted_events = array();
$actual_year = date('Y', time());
$actual_month = date('m', time());
$last_year = $last_month = null;
$events = get_posts(array('posts_per_page' => -1, 'post_type' => 'event', 'orderby' => 'date', 'order' => 'ASC', 'post_status' => array('future', 'publish')));

ob_start();

echo '<div class="calendar-container"><h2>Kalendarz</h2>' . "\n";

if (count($events)):
    foreach ($events as $event) {
        $post_date = preg_match('/([0-9]{4})-([0-9]{2})-([0-9]{2})/', $event->post_date, $matches);
        if ($last_year != $matches[1] || $last_month != $matches[2]) {
            if ($last_year != null && $last_month != null) {
                echo '</ul></div></div>' . "\n";
            }
            echo '<div class="calendar-block' .
                ($actual_year == $matches[1] && $actual_month == $matches[2] ? ' active-block' : '')
                . '">';
            echo '<h3>' . mysql2date('F Y', $event->post_date) . '</h3>' . "\n";
            echo '<div class="events-list-container"><ul>';
        }
        $link = get_post_meta($event->ID, 'url', true);
        echo '<li><span>' . $matches[3] . '.' . $matches[2] . ' </span><span class="mask"></span><span><a href="' .
            $link . '">' .
            $event->post_title . '</a></span></li>';
        $last_year = $matches[1];
        $last_month = $matches[2];
    }
    echo '</ul></div>';
endif;

echo '</div>'; //end calendar-container

$ob_content = ob_get_contents();
ob_end_clean();

include(get_stylesheet_directory() . '/template-about-lob.php');
