<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 27.08.13
 * Time: 15:01
 * To change this template use File | Settings | File Templates.
 */
/*
 * Template Name: Autoryzowane Materiały
 */
ob_start();

echo '<div>';
if (post_password_required()) {
    echo '<div class="page-popup-container">';
    echo '<div class="popup-mask"></div>';
    echo '<div class="popup-content">';
    echo '<div class="cell">';
    if ($_COOKIE['cookie_accept'] === 'true') {
        the_content();
    } else {
        ?>
        <form>
            <p>Musisz zakceptować ciasteczka jeśli chcesz mieć dostęp do materiałów</p>

            <p>
                <button class="cookie-accept-redirect">Akceptuj</button>
                <button class="cookie-back-redirect">Wstecz</button>
            </p>
        </form>
        <script type="text/javascript">
            (jQuery)(function ($) {
                $('.cookie-accept-redirect').on('click', function () {
                    var date = new Date();
                    date.setFullYear(date.getFullYear() + 1);
                    document.cookie = "cookie_accept=true;expires=" + date.toUTCString() + ";path=/;";
                    window.location = "<?php echo get_permalink($post->ID); ?>";
                });
                $('.cookie-back-redirect').on('click', function () {
                    window.history.back();
                    return false;
                });
            });
        </script>
    <?php
    }
    echo '</div>';
    echo '</div>';
    echo '</div>';

} else {
    echo '<h1>Materiały do pobrania</h1>';
    $args = array(
        'post_type' => 'attachment',
        'numberposts' => -1,
        'post_status' => 'any',
        'post_parent' => $post->ID,
        'post_mime_type' => 'application,text'
    );

    $attachments = get_posts($args);
    if ($attachments) {
        echo '<div class="private-materials">';
        foreach ($attachments as $attachment) {
            $url = get_permalink($attachment->ID);
            $title = apply_filters('the_title', $attachment->post_title);
            ?>
            <div class="attachment">
                <?php
                $label = $attachment->post_excerpt != '' ? $attachment->post_excerpt : $title;
                echo '<a href="' . $url . '" class="' . $attachment->post_mime_type . '">' .
                    '<img src="' . get_bloginfo('template_url') . '/images/btn-document-' . $post->ID . '.png" alt="' .
                    $label . '" /></a><p>' . $attachment->post_content . '</p>';
                ?>
            </div>
        <?php

        }
        echo '</div>';
    }
}
echo '</div>';
$ob_content = ob_get_contents();
ob_end_clean();

$parent_template = get_root_parent_template($post);
$path = get_stylesheet_directory() . '/' . $parent_template;
if (file_exists($path)) {
    include($path);
    exit;
}

