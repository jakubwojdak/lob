<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jakub
 * Date: 14.08.13
 * Time: 11:09
 * To change this template use File | Settings | File Templates.
 */
//standard page should be the same template like her root parent
global $root_parent_post;
$root_parent_post = $post;
while ($root_parent_post->post_parent > 0) {
    $root_parent_post = get_post($root_parent_post->post_parent);
    if ($root_parent_post->post_parent === 0) {
        $parent_template = get_post_template_by_id($root_parent_post->ID);
        if ($parent_template != null) {
            $path = get_stylesheet_directory() . '/' . $parent_template;
            if (file_exists($path)) {
                include($path);
                exit;
            }

        }
    }
}
